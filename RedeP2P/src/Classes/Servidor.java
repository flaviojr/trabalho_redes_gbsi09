package Classes;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class Servidor implements Runnable{
	private ServerSocket server;
	public static List<AuxiliaServer> lista_aux_server = new ArrayList<AuxiliaServer>();
	public static List<String> listaMensagem = new ArrayList<String>();
	public static List<Usuario> listaUsuarios = new ArrayList<Usuario>();
	private boolean inicializado;
	private boolean executando;
	private Thread thread;
	
	public Servidor(int porta) throws Exception{
		
		inicializado = false;
		executando = false;
		
		open(porta);
	}
	
	//Abre porta para receber novas conexoes
	private void open(int porta) throws Exception{
		server = new ServerSocket(porta);
		inicializado = true;
	}
	
	//Fecha todas as conexoes antes de finalizar o servidor
	private void close(){
		for(AuxiliaServer aux : lista_aux_server){
			try{
				aux.stop();
			}catch(Exception e){
				System.out.println(e);
			}
		}
		
		try{
			server.close();
		}
		catch(Exception e){
			System.out.println(e+" ERRO AO FECHAR SERVIDOR");
		}
		
		server = null;
		inicializado = false;
		executando = false;
		thread = null;
	}
	
	//Inicia a thread Servidor
	public void start(){
		if(!inicializado || executando){
			return;
		}
		executando = true;
		thread = new Thread(this);
		thread.start();
		
	}
	
	//Pausa a thread Servidor
	public void stop() throws Exception{
		
		executando = false;
		
		if(thread != null){
			thread.join();
		}
	}
	
	//Metodo responsavel por enviar mensagens broadcast, ele repassa a mensagem para todos os usuarios conectados nele
	//se houver usuario que saiu ou perdeu conexao ele retira da lista de usuarios conectados
	public static void broadcastMsg(JSONObject pacote){

		for(int i = 0; i < lista_aux_server.size(); i++){
			try{
				lista_aux_server.get(i).enviaMsg(pacote);
			}catch(Exception e){
				lista_aux_server.remove(lista_aux_server.get(i));  
			        --i;  
			    } 
				//System.out.println(e+" ERRO BROADCAST SERVIDOR");
			}
		}
		
	
	//Metodo que ficara executando aguardando novas conexoes
	@Override
	public void run(){
		System.out.println("Aguardando conex�o.");
		
		while(executando){
			try{
				server.setSoTimeout(2500);
				
				Socket socket = server.accept();
				
				AuxiliaServer aux = new AuxiliaServer(socket);
				aux.start();
				
				lista_aux_server.add(aux);
				
			}catch(SocketTimeoutException e){
				
			}catch(Exception e){
				System.out.println(e+" - ERRO SERVIDOR");
				break;
			}
		}
		close();
	}
	



}
