package Classes;
import java.util.Random;
import org.json.JSONObject;

public class Mensagem {

	
	public JSONObject buildMsg(String msg, String destinatario, String apelido){
		Random gerarNumero = new Random();
		String nMsg = ""+gerarNumero.nextInt(300);
		JSONObject my_obj = new JSONObject();
		my_obj.put("AUTENTICACAO", "0");
		my_obj.put("IDENTIFICADOR", nMsg);
		my_obj.put("MENSAGEM", msg);
		my_obj.put("APELIDO", apelido);
		my_obj.put("DESTINATARIO", destinatario);
		return my_obj;
	}
	
	public JSONObject autenticar(String apelido, String enderecoLocal, String porta){
		Random gerarNumero = new Random();
		String nMsg = ""+gerarNumero.nextInt(300);
		JSONObject my_obj = new JSONObject();
		my_obj.put("AUTENTICACAO", "1");
		my_obj.put("IDENTIFICADOR", nMsg);
		my_obj.put("DESTINATARIO", "");
		my_obj.put("MENSAGEM", apelido+" entrou na sala");
		my_obj.put("APELIDO", apelido);
		my_obj.put("ENDERECO LOCAL", enderecoLocal);
		my_obj.put("PORTA", porta);
		return my_obj;
	}

	public JSONObject msgListaUsuario(String apelido, String enderecoLocal, String porta){
		Random gerarNumero = new Random();
		String nMsg = ""+gerarNumero.nextInt(300);
		JSONObject my_obj = new JSONObject();
		my_obj.put("AUTENTICACAO", "2");
		my_obj.put("IDENTIFICADOR", nMsg);
		my_obj.put("DESTINATARIO", "");
		my_obj.put("MENSAGEM", "Online");
		my_obj.put("APELIDO", apelido);
		my_obj.put("ENDERECO LOCAL", enderecoLocal);
		my_obj.put("PORTA", porta);
		return my_obj;
	}
}
