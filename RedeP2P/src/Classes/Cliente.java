package Classes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONObject;


public class Cliente implements Runnable{
	private Socket socket;
	private BufferedReader in;
	private static PrintStream out;
	private boolean inicializado;
	private boolean executando;
	private Thread thread;
	public static Usuario user = new Usuario();
	
	public Cliente (String endereco, int porta) throws Exception{
		inicializado = false;
		executando = false;
		open(endereco, porta);
	}
	
	//Recupera o Ip da maquina
	public static String getIp() throws IOException{
		URL procurarIp = new URL("http://checkip.amazonaws.com");
		BufferedReader in = new BufferedReader(new InputStreamReader(
		procurarIp.openStream()));

		String ip = in.readLine();
		
		return ip;
	}
	//Cria conexao para o cliente usando o IP e porta passada
	private void open(String endereco, int porta) throws Exception{
		try{
		socket = new Socket(endereco, porta);
		
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintStream(socket.getOutputStream());
		inicializado = true;

		}catch(Exception e){
			System.out.println("Sua conex�o caiu...");
			close();
			throw e;
		}
	}
	
	//Fecha conexao do cliente
	private void close(){
		
		//Verifica se os valores s�o diferentes de NULL para fechar apenas se estiver aberto
		if(in != null){
			try{
				in.close();
			}catch(Exception e){
				System.out.println(e);
			}
		}
		
		if(out != null){
			try{
				out.close();
			}catch(Exception e){
				System.out.println(e);
			}
		}
		
		if(socket != null){
			try{
				socket.close();
			}catch(Exception e){
				System.out.println(e);
			}
		}
		
		in = null;
		out = null;
		socket = null;
		inicializado = false;
		executando = false;
		thread = null;
	}	
	
	//Inicia a thread cliente, antes verifica se n�o esta executando
	public void start(){
		if(!inicializado || executando){
			return;
		}
		
		executando = true;
		thread = new Thread(this);
		thread.start();
	}
	
	//Pausa a thread com o comando join, que aguarda at� que as outras threads parem de executar
	public void stop() throws Exception{
		executando = false;
		
		if(thread != null){
			thread.join();
		}
	}
	
	//Verifica se a thread est� executando
	public boolean isExecutando(){
		return executando;
	}
	
	//Repassa o pacote recebido para o usuario que ele esta conectado
	public  static void encaminha(String mensagem){
		try{
			out.println(mensagem);
		}catch(Exception e){

		}
	}
	
	//Envia mensagem para o usu�rio que ele esta conectado
	public  void send(String mensagem){
		try{
			out.println(mensagem);
		}catch(Exception e){

		}
	}
	
	/*Metodo que ser� executado ap�s a thread Cliente ser iniciada, este metodo ira receber todos os pacotes enviados pelo lado
	  servidor que ele estiver conectado, sera realizado diversas validacoes para receber mensagens publicas e privadas, assim como
	  mensagens do sistema, como para incluir um novo usuario na lista, apos receber uma mensagem ele repassa via broadcast para outros usuarios*/
	@Override
	public void run(){
		while(executando){
			try{
				socket.setSoTimeout(2500);
				String mensagem = in.readLine();
				
				if(mensagem == null){
					break;
				}
				
				JSONObject pacoteRecebido = new JSONObject(mensagem);
				
				if(!Servidor.listaMensagem.contains(pacoteRecebido.getString("IDENTIFICADOR"))){
					if(pacoteRecebido.getString("MENSAGEM").equals("sair")){
						//Se recebeu uma mensagem com a palavra sair, remove da lista o usuario que enviou a mensagem e repassa a nova lista
						//para a execucao deste usuario logo depois de enviar a lista de usuarios

								for(int i = 0; i < Servidor.listaUsuarios.size(); i++){
		
									if(Servidor.listaUsuarios.get(i).getApelido().equals(pacoteRecebido.getString("APELIDO"))){
									Servidor.listaUsuarios.remove(Servidor.listaUsuarios.get(i));  
									--i;  
									}
									}
								//AuxiliaServer.enviarListaUsuario();		
					}	
					
					if(!pacoteRecebido.getString("DESTINATARIO").isEmpty()){
						if(pacoteRecebido.getString("DESTINATARIO").equals(user.getApelido())){
							Servidor.listaMensagem.add(pacoteRecebido.getString("IDENTIFICADOR"));
							System.out.println(pacoteRecebido.getString("APELIDO")+" : "+pacoteRecebido.getString("MENSAGEM"));
						}else{
							  Servidor.listaMensagem.add(pacoteRecebido.getString("IDENTIFICADOR"));
							  Servidor.broadcastMsg(pacoteRecebido);
						}
					}
					else if(pacoteRecebido.getString("AUTENTICACAO").equals("2")){
						int ck = 1;
						String n = pacoteRecebido.getString("APELIDO");
						for(Usuario aux : Servidor.listaUsuarios){
							if(aux.getApelido().equals(n)){
								ck = 0;
							}
					}
						if(ck == 1){
							Usuario novo = new Usuario();
							novo.setApelido(pacoteRecebido.getString("APELIDO"));
							novo.setEnderecoLocal(pacoteRecebido.getString("ENDERECO LOCAL"));
							novo.setPorta(pacoteRecebido.getString("PORTA"));
							Servidor.listaUsuarios.add(novo);
						}
				}
					else{
					Servidor.listaMensagem.add(pacoteRecebido.getString("IDENTIFICADOR"));
					System.out.println(pacoteRecebido.getString("APELIDO")+" : "+pacoteRecebido.getString("MENSAGEM"));
					
					Servidor.broadcastMsg(pacoteRecebido);
					}
				}
				
			}catch(SocketTimeoutException e){

				
			}catch(Exception e){
				System.out.println(e+" - PAROU DE EXECUTAR CLIENTE");
				break;
			}
		}
		
		close();
	}
	

	
	//Classe principal, nela sera realizado o inicio da aplicacao
	public static void main(String[] args) throws Exception{
	
	Scanner scanner = new Scanner(System.in);
	Mensagem msg = new Mensagem();
	
	JSONObject msgAutenticar = new JSONObject();
	
	try{
		user.setEnderecoLocal(getIp());
		}catch(Exception e){
			System.out.println("Nao foi possivel identificar IP ERRO - "+e);
		}
	
	System.out.println("Seu Apelido:");
	user.setApelido(scanner.nextLine());
	
	System.out.println("Informe a porta para o servidor:");
	user.setPorta(scanner.nextLine());	
	
	System.out.println("Iniciando Servidor...");
	Servidor servidor = new Servidor(Integer.parseInt(user.getPorta()));
	servidor.start();
	
	System.out.println("Informe o IP para conex�o sem a porta: ");
	
	String enderecoIp = scanner.nextLine();
	
	System.out.println("Informe a PORTA para o IP: "+enderecoIp);
	int porta = Integer.parseInt(scanner.nextLine());	

	Servidor.listaUsuarios.add(user);
	
	System.out.println("Iniciando Conexao com o endere�o: "+enderecoIp+":"+porta);
	Cliente client = new Cliente(enderecoIp, porta);
	
	System.out.println("Conex�o estabelecida com sucesso.");


	msgAutenticar = msg.autenticar(user.getApelido(), user.getEnderecoLocal(), user.getPorta());
	
	client.send(msgAutenticar.toString());
	
	msgAutenticar = msg.msgListaUsuario(user.getApelido(), user.getEnderecoLocal(), user.getPorta());
	
	Servidor.broadcastMsg(msgAutenticar);

	client.start();
	
	String mensagem = "";
	
	while(true){

			if(!client.isExecutando()){
				client.close();
				for(Usuario aux : Servidor.listaUsuarios){
					if(!client.isExecutando()){
					if(aux.getApelido() != user.getApelido()){
						enderecoIp = aux.getEnderecoLocal(); 
						porta = Integer.parseInt(aux.getPorta());
						try{
							client = new Cliente(enderecoIp, porta);
							client.start();
						}catch(Exception e){
							System.out.println("...Procurando Conex�o...");
						}
					}
				}
				}
				if(!client.isExecutando()){
					System.out.println("Sem conex�o.");
					mensagem = "sair";
					break;
				}
			}
			
			System.out.println("Mensagem privada: -p | Listar usuarios: -l");
			mensagem = scanner.nextLine();
			
			JSONObject pacote = new JSONObject();
			
			//Se a mensagem fo -p significa q sera uma mensagem privada
			if(mensagem.equals("-p")){
				
				//Recebe o destinatario da mensagem
				System.out.print("DESTINATARIO: ");
				String destinatario = scanner.nextLine();
				
				//Recebe a mensagem
				System.out.println("Mensagem: ");
				mensagem = scanner.nextLine();
				
				//Cria um pacote json para enviar mensagem privada
				pacote = msg.buildMsg(mensagem, destinatario, user.getApelido());
				
				//Envia Mensagem para o usuario conectado
				client.send(pacote.toString());
				
				//Envia broadcast para os conectados a ele
				Servidor.broadcastMsg(pacote);
			
				//Se a mensagem for -l ira listar todos os usuarios da rede
			}else if(mensagem.equals("-l")){
				System.out.println("Listando Usu�rios Online:");
				for(Usuario aux : Servidor.listaUsuarios){
					System.out.println("Apelido: "+aux.getApelido()+" - IP: "+aux.getEnderecoLocal()+" - Porta: "+aux.getPorta());
				}
			}
			else{
				//Cria JSON de mensagem publica
			    	pacote = msg.buildMsg(mensagem, "", user.getApelido());
			    	
			    	Servidor.listaMensagem.add(pacote.getString("IDENTIFICADOR"));
			    	
					//Envia Mensagem para o usuario conectado
					client.send(pacote.toString());
					
					//Envia broadcasst para os conectados a ele
					Servidor.broadcastMsg(pacote);
			}
			//Se a mensagem for sair, o sistema sai do laco de repeticao e para o cliente
			if(mensagem.equals("sair")){
				break;
			}
		}
	if(mensagem.equals("sair")){
		System.out.println("Saindo.");
		client.stop();
		System.out.println("Voc� est� offline.");
		servidor.stop();
	}
	}
}
