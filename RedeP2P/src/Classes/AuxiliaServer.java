package Classes;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

import org.json.JSONObject;


public class AuxiliaServer implements Runnable{
	private Socket socket;
	private BufferedReader in;
	private static PrintStream out;
	private boolean inicializado;
	private boolean executando;
	private Thread thread;

	public AuxiliaServer(Socket socket) throws Exception{
		this.socket = socket;
		this.inicializado = false;
		this.executando = false;
	
		open();
	}
	
	//Este metodo abre uma conexao para receber e enviar mensagens atraves  do servidor
	private void open() throws Exception{
		try{
		//Recebe mensagens
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
		//Envia mensagens
		out = new PrintStream(socket.getOutputStream());
		inicializado = true;
		}catch(Exception e){
			close();
			throw e;
		}
		
	}
	
	//Fecha conexao impedindo enviar ou receber conexoes
	private void close(){
		if(in != null){
			try{
				in.close();
			}catch(Exception e){
				System.out.println(e);
			}
		}
		
		if(out != null){
			try{
				out.close();
			}catch(Exception e){
				System.out.println(e);
			}
		}
		
		try{
			socket.close();
		}catch(Exception e){
			System.out.println(e);
		}
		
		in = null;
		out = null;
		socket = null;
		
		inicializado = false;
		executando = false;
		thread = null;
	}
	
	//Inicializa thread
	public void start(){
		if(!inicializado || executando){
			return;
		}
		executando = true;
		thread = new Thread(this);
		thread.start();
	}
	
	//Pausa a thread at� que as outras threads parem de executar
	public void stop() throws Exception{
		executando = false;
		
		if(thread != null){
			thread.join();
		}
	}
	
	//Envia mensagem para usuario conectado no servidor
	public void enviaMsg(JSONObject pacote){
		out.println(pacote);
	}
	
	//Envia todos os usuarios da lista
	public static void enviarListaUsuario(){
		Mensagem msg = new Mensagem();
		JSONObject pacote = new JSONObject();
		for(Usuario aux : Servidor.listaUsuarios){
			pacote = msg.msgListaUsuario(aux.getApelido(), aux.getEnderecoLocal(), aux.getPorta());
			try{
			out.println(pacote.toString());
			}catch(Exception e){
				System.out.println(e+" Erro ao enviar lista de usuarios");
			}
		}
	}
	
	/*Metodo responsavel por receber e repassar todos os pacotes recebidos no servidor
	  Ele repassa os pacotes e envia pacotes, assim como a lista de usuarios atualizada*/
	@Override
	public void run(){
		while(executando){
			try{
			//Tempo de vida que o socket ira aguardar
			socket.setSoTimeout(2500);
			String mensagem = in.readLine();
			
			JSONObject pacoteRecebido = new JSONObject(mensagem);
			
			if(pacoteRecebido.getString("MENSAGEM").equals("sair")){
				//Se recebeu uma mensagem com a palavra sair, remove da lista o usuario que enviou a mensagem e repassa a nova lista
				//para a execucao deste usuario logo depois de enviar a lista de usuarios
				for(int i = 0; i < Servidor.listaUsuarios.size(); i++){
					
					if(Servidor.listaUsuarios.get(i).getApelido().equals(pacoteRecebido.getString("APELIDO"))){
					Servidor.listaUsuarios.remove(Servidor.listaUsuarios.get(i));  
					--i;  
					}
					}
						//enviarListaUsuario();		

				break;
			}	
			
		if(!Servidor.listaMensagem.contains(pacoteRecebido.getString("IDENTIFICADOR"))){
			//socket.getInetAddress().getHostName() --  socket.getPort()
			
			if(!pacoteRecebido.getString("DESTINATARIO").isEmpty()){
				if(pacoteRecebido.getString("DESTINATARIO").equals(Cliente.user.getApelido())){
					Servidor.listaMensagem.add(pacoteRecebido.getString("IDENTIFICADOR"));
					System.out.println(pacoteRecebido.getString("APELIDO")+" : "+pacoteRecebido.getString("MENSAGEM"));
				}else{
				Servidor.listaMensagem.add(pacoteRecebido.getString("IDENTIFICADOR"));
				Servidor.broadcastMsg(pacoteRecebido);
				Cliente.encaminha(pacoteRecebido.toString());
				}
			}else{
				if(pacoteRecebido.getString("AUTENTICACAO").equals("1")){
					String nome = pacoteRecebido.getString("APELIDO");
					int check = 1;
					for(Usuario aux : Servidor.listaUsuarios){
						
						if(aux.getApelido().equals(nome)){
							check = 0;
							pacoteRecebido.put("MENSAGEM", "FATAL ERROR 1045");
						}
						
					}
					if(check == 1){
						Usuario novo = new Usuario();
						novo.setApelido(pacoteRecebido.getString("APELIDO"));
						novo.setEnderecoLocal(pacoteRecebido.getString("ENDERECO LOCAL"));
						novo.setPorta(pacoteRecebido.getString("PORTA"));
						Servidor.listaUsuarios.add(novo);
						pacoteRecebido.put("AUTENTICACAO", "2");
						
						//Enviar todos os usuarios da lista
						enviarListaUsuario();
					}
					
				}else if(pacoteRecebido.getString("AUTENTICACAO").equals("2")){
					int ck = 1;
					String n = pacoteRecebido.getString("APELIDO");
					for(Usuario aux : Servidor.listaUsuarios){
						if(aux.getApelido().equals(n)){
							ck = 0;
						}
				}
					if(ck == 1){
						Usuario novo = new Usuario();
						novo.setApelido(pacoteRecebido.getString("APELIDO"));
						novo.setEnderecoLocal(pacoteRecebido.getString("ENDERECO LOCAL"));
						novo.setPorta(pacoteRecebido.getString("PORTA"));
						Servidor.listaUsuarios.add(novo);
					}
			}
			if(pacoteRecebido.getString("MENSAGEM").equals("FATAL ERROR 1045")){
				Mensagem msgError = new Mensagem();
				pacoteRecebido = msgError.buildMsg("APELIDO JA CADASTRADO", pacoteRecebido.getString("APELIDO"), "SISTEMA");
				Servidor.broadcastMsg(pacoteRecebido);
				
				break;
			}
			Servidor.listaMensagem.add(pacoteRecebido.getString("IDENTIFICADOR"));

			System.out.println(pacoteRecebido.getString("APELIDO")+" : "+pacoteRecebido.getString("MENSAGEM"));
			
			//Envia broadcast e encaminha o pacote para o usuario que ele estabeleceu conexao
			Servidor.broadcastMsg(pacoteRecebido);
			Cliente.encaminha(pacoteRecebido.toString());
			
			}
		}
		}catch(SocketTimeoutException e){
		}catch(Exception e){
			System.out.println(e);
			break;
		}
	
	}
		close();
	}
}
