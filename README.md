# README #

#Executar o arquivo .JAR#
* Abrir o PROMPT.
* Ir até a pasta onde se encontra o arquivo .JAR
* Executar o seguinte comando: java -jar nomeDoArquivo.jar

### Siga os seguintes passos para utilizar o chat. ###
* Você precisa ter 2 computadores conectados na internet, ou executar duas vezes a Classe Cliente para trocar mensagens
* A Classe Cliente possue o main
* Ao executar informe o Apelido para logar no chat
* Em seguida informe uma porta para receber conexões
* Logo depois será solicitado o endereço IP de outro usuário que também esta utilizando o CHAT, informe apenas o IP sem a porta
* Depois informe a porta desse usuario
* Pronto! Você pode mandar mensagens e este usuário, poderá ler tudo, mas ele só irá responder se estiver conectar com alguém

### Comandos básicos ###
* Digite -p para enviar mensagem Privada
* Passe o Destinatario e depois a mensagem
* Digite -l para listar todos os usuários do Chat
* Digite sair para sair do Chat